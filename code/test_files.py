import pandas as pd
import gitlab
import os
import sys

# access token
tok = os.getenv('CI_ACCESS_TOKEN_OS')

df = pd.read_csv('data/usernames.csv')
usernames = df['username'].tolist()

gl = gitlab.Gitlab('https://gitlab.uzh.ch', private_token=tok, per_page=100)
project = gl.projects.get("open_science_course/apitest")

items = project.repository_tree(path='homework')
allhws=[items[i]["name"] for i in range(len(items))]

expected_filenames=[i+".md" for i in usernames]
for ef in expected_filenames:
  def test_if_file_present():
    assert ef in allhws

