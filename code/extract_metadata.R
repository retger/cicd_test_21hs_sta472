utilsdirfile <- ifelse(interactive(), here::here("code","utils.R"), "/R/code/utils.R")
source(utilsdirfile)

files <- get_all_files()
build_all_Rmd(files[2])

#in terminal:
# pandoc -f markdown -t json -o README.json README.md
md_file <- stringr::str_replace(files[2],"\\.Rmd$","\\.md")
json_file <- stringr::str_replace(md_file,"\\.md$","\\.json")
system(paste("pandoc -f markdown -t json -o",json_file,md_file))

rm_ast <- jsonlite::fromJSON(json_file, simplifyVector = FALSE)
xfun:::tree(rm_ast)
#
# if(rm_ast$meta$author$t == "MetaList"){
#   author_ls_ls <- rm_ast$meta$author$c
#   all_authors <- list()
#   for(i in seq_along(author_ls_ls)){
#     author_ls <- rm_ast$meta$author$c[[i]]$c
#     length(author_ls)
#     author_c_ls <-  purrr::map(author_ls, ~.x$c)
#     all_authors[[i]] <- unlist(author_c_ls[purrr::map_lgl(author_c_ls, ~!is.null(.x))])
#   }
#
# } else{
#   author_ls <- rm_ast$meta$author$c
#   length(author_ls)
#   author_c_ls <-  purrr::map(author_ls, ~.x$c)
#   all_authors <- list(unlist(author_c_ls[purrr::map_lgl(author_c_ls, ~!is.null(.x))]))
# }


yaml_h <- get_yaml_header(files[2])

yaml_h$author
